# Configuration Builder and Tester
`amiroCI` is a testing tool written for the [AMiRo-OS](https://gitlab.ub.uni-bielefeld.de/AMiRo/AMiRo-OS.git) and [AMiRo-Apps](https://gitlab.ub.uni-bielefeld.de/AMiRo/AMiRo-Apps.git) project.
Goal is to utilize a configuration file that contains all desired parameters that should be tested, and apply those with the help of conditional compilation.
The results of all compile processes are gathered in a single report file,
presenting the used parameter configuration as well as _error_, _warn_ and _note_ messages.
This report can be compared to an existing one which generates an comparison containing only those compilations that does not exist or match those in the latter report.
The [Wiki](../../wikis/home) contains more detailed information about the capabilities and how
to use `amiroCI`.

## Usage with GitLab CI 
_The following steps require you to have Maintainer Access to your repository_

1. Add this repository's server as known host
    - You need to store its public key in the GitLab variable used by `.gitlab-ci.yml`
    - [Create a variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) called `SSH_KNOWN_HOSTS` and past the following content:
        ```
        gitlab.ub.uni-bielefeld.de ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCufQV6CCSOdr9y3k61JY2tTAERqSaqq692q+Pfrq8r0oPIUBaTpFP3aJNEFipVqpgQSxLnHni8BH0FpoCYY2H7XXlmplW4B42VoGBmrEzJa3ya2xQqrPaqqXTbAxdqVcdatgt5uG1TbQj28oKnYexanGhxbGJLkcKIW3KyfUxq1Q+5motxBOELFCVajFuCSPTz7NNypKM+fTG8Z33+mom7KA4aDvan/vhqy3NfR/FS4wFYo6O/sKJ4BYit+4CZE4ZbfjODitlYCQ0J10+37rxZ3F4bQv6QXaeNKG7qDy3BVEjxa5tUATTt9j+kqOSDFoqJD2sedEd1tbRVs4ZCdqRt
        ```
1. Obtain a __deploy key__ (read-only) for the [AMiRo-CI](https://gitlab.ub.uni-bielefeld.de/AMiRo/amiroci) repository
    - This allows cloning of the repositories contents
    - Only members with __Maintainer__ access to the repository are able to create deploy keys
    - Obtain the private part of the key and [store it in a variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) called `AMIRO_CI_DEPLOY_PRIVATE`
1. Add the CI configuration file to your repository 
    - Download the `.gitlab-ci.yml` from this repository:
        ```
        curl https://gitlab.ub.uni-bielefeld.de/AMiRo/amiroci/-/raw/master/.gitlab-ci.yml
        ```
    - Or merge its content with an already existing CI config
1. Schedule the pipeline execution
    - The `.gitlab-ci.yml` configures two execution stages, one will run with every commit and one with run only when triggered by a schedule
    - [Add a pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) with your desired execution interval
1. Specify the configs to use
    - Two separate CI/CD Variables define the configs used for CI/CD runs triggered by commits or schedules
    - [Create variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) `SCHEDULE_PIPELINE_CONFIG` pointing to configs you want to use for scheduled execution
       - Exemple:`https://gitlab.ub.uni-bielefeld.de/AMiRo/amiroci/-/raw/master/templates/repl_conf_aos.yml` 


## Manual Setup
```bash
pip install git+https://gitlab.ub.uni-bielefeld.de/AMiRo/amiroci.git
```

### Recommended Environment Setup
In order to test the Amiro-OS or Amiro-Apps the root needs to be provided.
This can be achieved on two different ways, with the CLI
or with environment variables:

```bash
export AOS_ROOT=path/to/Amiro-os
export AOS_APPS_ROOT=path/to/Amiro-apps
export AOS_REPLACE_CONF=path/to/replconf.yaml
```
The environment variables are interpreted as default but can be overwritten with the CLI.

### Execution

``` bash
amiroCI --help
```
For more detailed usage examples take a look at [CLI Usage](https://gitlab.ub.uni-bielefeld.de/AMiRo/amiroci/-/wikis/CLI%20Usage).

<!-- ## General Architecture -->
<!-- <img src="assets/architecture.png" -->
<!--      alt="Architecture" -->
<!--      style="float: left; margin-right: 10px;" /> -->

<!-- ## Search Module -->
<!-- ## Configuration Module -->
<!-- ## AutoCompile Module -->
<!-- ## Reporter Module -->
<!-- ## CLI -->
