### header printing
function header() {
  echo -e "\e[34m"
  echo -e "================================================================================"
  echo -e "##"
  echo -e "##  $1"
  echo -e "##"
  echo -e "================================================================================"
  echo -e "\e[39m"
}

function show_help() {
  echo -e "Hier könnte Ihre Hilfe stehen!"
}

### commandline argument parsing
function parse_args() {
  header "Parsing arguments"
  while getopts h:c:l:d flag
  do
      case "${flag}" in
          h) 
            show_help
            exit 0
            ;;
          c) AOS_REPLACE_CONF=${OPTARG};;
          l) ARM_GCC_LINK=${OPTARG};;
          d) ONLY_DEFAULTS=true;;
          *)
            show_help
            exit 0
            ;;
      esac
  done
  export AOS_REPLACE_CONF
  export ARM_GCC_LINK
  export ONLY_DEFAULTS
}

### setup amiro-apps
function amiroappsSetup() {
  header "Setting up the amiro apps repository and initializing..."
  if [[ -d "amiro-apps" ]]; then rm -Rf amiro-apps; fi
  git clone git@gitlab.ub.uni-bielefeld.de:AMiRo/AMiRo-Apps.git amiro-apps
  cd amiro-apps || {
    echo "Could not clone amiro-apps repository, exiting.."
    exit 1
  }
  git submodule update --init --recursive
  ./os/AMiRo-OS/kernel/kernelsetup.sh --patch --quit
  cd ..
}

### compiler setup
function compilerSetup() {
  header "Setting up the GCC compiler..."
  if [[ -z ${ARM_GCC_LINK} ]]; then 
    echo "Using default gcc download link as nothing else was specified"
    ARM_GCC_LINK="https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2"
    export ARM_GCC_LINK
  fi
  link="${ARM_GCC_LINK}"
  tarball="gcc.tar.bz2"
  compilerdir="gcc"
  if [[ -d "$compilerdir" ]]; then rm -Rf "$compilerdir"; fi
  if [[ -d "$tarball" ]]; then rm -Rf "$tarball"; fi
  wget -nv "$link" -O "$tarball"
  tar -jxf "$tarball"
  extractdir=$(tar --bzip2 -tf ${tarball} | sed -e 's@/.*@@' | uniq)
  mv "$extractdir" "$compilerdir"
  compilerpath=$(readlink -m "$compilerdir"/bin)
  export PATH=$PATH:"$compilerpath"
}

### amiro-ci installation
function preparePythonEnv() {
  header "Preparing the python environment..."
  if [[ -d "venv" ]]; then rm -Rf venv; fi
  python3.9 -m venv venv && source venv/bin/activate
  pip install git+https://gitlab.ub.uni-bielefeld.de/AMiRo/amiroci.git
}

### amiro-ci execution
function amiroCi() {
  header "Running the amiro_ci stuff..."
  echo "${AOS_REPLACE_CONF}"
  if [[ -z ${AOS_REPLACE_CONF} ]]; then 
    echo "Using default replace conf as nothing else was specified"
    AOS_REPLACE_CONF=$(readlink -m "../repo/templates/repl_conf.yml")
    export AOS_REPLACE_CONF
  fi
  AOS_APPS_ROOT=$(readlink -m "amiro-apps")
  AOS_ROOT=$(readlink -m "amiro-apps/os/AMiRo-OS")
  export AOS_APPS_ROOT
  export AOS_ROOT
  amiroCI --aos -e
}

### run amiro make procedure with default values
function amiroDefault() {
  header "Building amiro with default parameters..."
  cd amiro-apps/os/AMiRo-OS || {
    echo "Could not find amiro-os installation, exiting..."
    exit 1
  }
  make all
  cd ../../..
  # todo: execute in amiro-apps if thats the target
}

### run the desired amiro configuration(s)
function execute() {
  if [[ $ONLY_DEFAULTS = true ]]; then
    amiroDefault
  else
    preparePythonEnv
    amiroCi
  fi
}


parse_args $@
amiroappsSetup
compilerSetup
execute
